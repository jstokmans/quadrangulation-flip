#include <stdio.h>
#include <vector>
#include <iostream>
#include <time.h>
#include <queue>
#include <sstream>
#include <fstream>
#include <string>
#include <set>
#include <algorithm>
#include <cmath>
#include <map>

#include "random.h"

#include "planarmap modified.h"

using namespace planar;

struct EdgeData {
	int vertexId;
	int distance;
	EdgeData(){};
};

typedef typename Map<EdgeData>::EdgeHandle EdgeHandle;

double degreeToMassRatio(std::vector<int> vertexDegrees, std::vector<int> vertexMasses)
{
    double DMRatio=0.0;
    for(int i=0; i<int(vertexDegrees.size()); i++)
    {
        DMRatio += vertexDegrees[i]/vertexMasses[i];
    }
    return DMRatio;
}

double squareSumDegree(std::vector<int> vertexDegrees, Map<EdgeData> & map)
{
    // Calculate the sum of the square degrees of all vertices
    double sum=0;
    for(int i=0; i<map.numVertices(); i++)
    {
        sum += vertexDegrees[i]*vertexDegrees[i];
    }
    return sum;
}

int maxVertexDegree(std::vector<int> vertexDegrees)
{
    // find the maximum vertex degree
    int maxDegree = 0;
    for(int i=0; i<int(vertexDegrees.size()); i++)
    {
        if(maxDegree<vertexDegrees[i]){maxDegree = vertexDegrees[i];}
    }
    return maxDegree;
}

int numVerticesOfMass(std::vector<int> vertexMasses, int mass=4)
{
    // find the number of vertices with mass 'mass'.
    int numVert = 0;
    for(int i=0; i<int(vertexMasses.size()); i++)
    {
        if(vertexMasses[i] == mass){numVert++;}
    }
    return numVert;
}

double squareSumMass(std::vector<int> vertexMasses, Map<EdgeData> & map)
{
    // Calculate the average value of the squares of all vertex masses
    double sum=0;
    for(int i=0; i<map.numVertices(); i++)
    {
        sum += vertexMasses[i]*vertexMasses[i];
    }
    return sum;
}

int maxVertexMass(std::vector<int> vertexMasses)
{
    // find the maximum vertex mass
    int maximum = 0;
    for(int i=0; i<int(vertexMasses.size()); i++)
    {
        if(maximum<vertexMasses[i]){maximum = vertexMasses[i];}
    }
    return maximum;
}

int numVerticesOfDegree(std::vector<int> vertexDegrees, int deg=4)
{
    // find the number of vertices with degree deg
    int numVert = 0;
    for(int i=0; i<int(vertexDegrees.size()); i++)
    {
        if(vertexDegrees[i] == deg){numVert++;}
    }
    return numVert;
}

template <typename T>
double sampleMean(std::vector<T> observable)
{
    double observableSum=0;
    for(int i=0; i<int(observable.size()); i++)
    {
        observableSum += (double)(observable[i]);
    }
    double mean = observableSum/(double)(observable.size());
    return mean;
}

template <typename T>
std::vector<double> autocovariance(std::vector<T> & observable, int numDataPoints)
{
    // determine the sample mean first
    double mean = sampleMean(observable);

    std::vector<double> autocov(numDataPoints,0);

    // calculate the autocovariance for each sweep
    for(int i=0; i<numDataPoints; i++)
    {
        for(int j=0; j<numDataPoints-i; j++)
        {
            autocov[i] += (observable[j]-mean)*(observable[j+i]-mean);
        }
        autocov[i] = autocov[i]/(numDataPoints-i);
    }

    return autocov;
}

template <typename T>
int autocorrTime(std::vector<T> observable, int numDataPoints)
{
    std::vector<double> autocov = autocovariance(observable, numDataPoints);

    // determine at what sweep the autocorrelation goes below 1/e
    for(int i=0; i<numDataPoints; i++)
    {
        if(autocov[i]<(autocov[0]/exp(1))){return i;}
    }
    return numDataPoints;
}

template <typename T>
double statError(std::vector<T> observable, double autocorrtime, double autocov0)
{
    int N = observable.size();
    return sqrt(2*autocorrtime*autocov0/N);
}

double WeightRatio(int l, int f, int a)
{
    //compute the ratio C_{l+/-1,f}/C_{l,f}
    if(a==1){return double(2*l+3*f-3)/(2*l+f+1) * 2*double(2*l+1)/l;}
    else{return double(2*l+f-1)/(2*l+3*f-5) * double(l-1)/(2*(2*l-1));}
}

// calculate the Metropolis-Hastings acceptance probability for the flip move
double acceptanceProbability(std::vector<int> & vertexDegrees, std::vector<int> & vertexMasses, int id1, int id2, int id3, int id4)
{
    std::vector<double>w(4);
    w[0] = WeightRatio(vertexDegrees[id1], vertexMasses[id1], -1);
    w[1] = WeightRatio(vertexDegrees[id2], vertexMasses[id2], -1);
    w[2] = WeightRatio(vertexDegrees[id3], vertexMasses[id3], 1);
    w[3] = WeightRatio(vertexDegrees[id4], vertexMasses[id4], 1);

    return w[0]*w[1]*w[2]*w[3];
}

double acceptanceProbabilityMassMove(std::vector<int> & vertexDegrees, std::vector<int> & vertexMasses, int id1, int id2, int delta, bool flipMass=false)
{
    int l,f,k,g;
    l=vertexDegrees[id1];
    f=vertexMasses[id1];
    k=vertexDegrees[id2];
    g=vertexMasses[id2];

    // compute the acceptance probability for a mass move of size 'delta'.
    if(!flipMass)
    {
        if(l+1.5*(f-delta-1)<0 || k+1.5*(g+delta-1)<0 || 1.5*(g-1)+k<0 || 1.5*(f-1)+l<0 || l+.5*(1+f-delta)<0){return 0.0;}
        double prefactor = lgamma(f+1)+lgamma(g+1)-lgamma(f-delta+1)-lgamma(g+delta+1);
        double acceptProb = (lgamma(k+.5*(1+g))+lgamma(l+.5*(1+f))+lgamma(l+1.5*(f-delta-1))+lgamma(k+1.5*(g+delta-1)))-
                            (lgamma(1.5*(g-1)+k)+lgamma(1.5*(f-1)+l)+lgamma(l+.5*(1+f-delta))+lgamma(k+.5*(1+g+delta)));
        return (1+floor(f/2))/(1+floor((g+delta)/2)) * exp(prefactor+acceptProb);
    }

    // compute the acceptance probability for a move exchanging two masses
    else
    {
        if(3*(g-1)/2+k<0 || 3*(f-1)/2+l<0 || 3*(f-1)/2+k<0 || 3*(g-1)/2+l<0){return 0.0;}
        double acceptProb = (lgamma(.5*(1+f)+l)+lgamma(.5*(1+g)+k)+lgamma(1.5*(f-1)+k)+lgamma(1.5*(g-1)+l))-
                            (lgamma(.5*(1+f)+k)+lgamma(.5*(1+g)+l)+lgamma(1.5*(g-1)+k)+lgamma(1.5*(f-1)+l));
        return exp(acceptProb);
    }
}

// select a random vertex uniformly from the map
template<typename RNG>
int randomVertex(RNG & rng, int numVertices)
{
    return uniform_int(rng, numVertices);
}

bool zeroMass(int* ptr)
{
    return *ptr==0;
}

// move mass between two vertices
template<typename RNG>
bool moveMass(std::vector<int> & vertexDegrees, std::vector<int> & vertexMasses, RNG & rng, double & accepted,
              double & largeAccept, int & largeAttempt, int startMass)
{
    // make a copy of the vertex masses containing pointers. This is useful when searching for a second vertex with nonzero mass.
    std::vector<int*> trimmedVertices(vertexMasses.size());
    for(int i=0; i<int(vertexMasses.size()); i++)
    {
        trimmedVertices[i] = &vertexMasses[i];
    }

    // select a random vertex
    int id1, id2;
    id2 = randomVertex(rng, vertexDegrees.size());

    // remove vertex corresponding to id2 from pool of possible vertices
    trimmedVertices.erase(trimmedVertices.begin()+id2);
    // remove all vertices with mass 0 from pool of possible vertices
    trimmedVertices.erase(std::remove_if(trimmedVertices.begin(), trimmedVertices.end(), zeroMass), trimmedVertices.end());

    // in case id2 corresponds to the only vertex with nonzero mass, make this the donating vertex and select another random vertex.
    if(trimmedVertices.size()==0)
    {
        id1 = id2;
        do
        {
            id2 = randomVertex(rng, vertexMasses.size());
        }while(id1==id2);
    }
    else
    {
        // Randomly select second vertex with nonzero mass different from the first.
        id1 = randomVertex(rng, trimmedVertices.size());
        int* p = trimmedVertices[id1];
        id1 = find(vertexMasses.begin(), vertexMasses.end(), *p)-vertexMasses.begin();
    }

    // Check whether the proposed move involves a vertex with a large mass
    double largeTreshold = ((double)(startMass)/(double)(vertexMasses.size())*10);
    if(vertexMasses[id1]>largeTreshold || vertexMasses[id2]>largeTreshold){largeAttempt++;}

    // Choose massChange uniformly and such that it is at least 1.
    int massChange = uniform_int(rng, floor(double(vertexMasses[id1])/2)+1)+1;

    // Attempt to move a mass massChange from vertex id1 to vertex id2.
    double acceptProb = acceptanceProbabilityMassMove(vertexDegrees, vertexMasses, id1, id2, massChange);
    if((acceptProb >= 1) || (uniform_real(rng, 0.0, 1.0) < acceptProb))
    {
        // update number of accepted moves
        accepted++;
        if(vertexMasses[id1]>largeTreshold || vertexMasses[id2]>largeTreshold){largeAccept++;}

        // move the mass
        vertexMasses[id1] -= massChange;
        vertexMasses[id2] += massChange;
        return true;
    }

    // if moving a mass massChange fails, attempt to exchange the masses of the two vertices
    acceptProb = acceptanceProbabilityMassMove(vertexDegrees, vertexMasses, id1, id2, 1, true);
    if((acceptProb >= 1) || (uniform_real(rng, 0.0, 1.0) < acceptProb))
    {
        // update number of accepted moves
        accepted++;
        if(vertexMasses[id1]>largeTreshold || vertexMasses[id2]>largeTreshold){largeAccept++;}

        // move the mass
        int massDump = vertexMasses[id1];
        vertexMasses[id1] = vertexMasses[id2];
        vertexMasses[id2] = massDump;
    }
    return true;
}

// perform edge flip in map (returns true if successful)
template<typename RNG>
bool flipEdge(Map<EdgeData> & map, EdgeHandle & edge, RNG & rng,
              std::vector<int> & vertexDegrees, std::vector<int> & vertexMasses, double & accepted)
{
	EdgeHandle adjEdge = edge->getAdjacent();

	int id1, id2, id3, id4;
	// get the vertex id's of the vertices connected by edge
	id1 = edge->data().vertexId;
	id2 = adjEdge->data().vertexId;

    // perform one of two possible flips with equal probability
    if(uniform_real(rng, 0.0, 1.0)<.5){
        // get the vertex id's of the target vertices (where edge will begin and end after the flip)
        id3 = edge->getNext()->getNext()->data().vertexId;
        id4 = edge->getAdjacent()->getNext()->getNext()->data().vertexId;
        double acceptProb = acceptanceProbability(vertexDegrees, vertexMasses, id1, id2, id3, id4);

        if(acceptProb >= 1 || uniform_real(rng, 0.0, 1.0) < acceptProb)
        {
            // update degrees of affected vertices
            vertexDegrees[id1]--;
            vertexDegrees[id2]--;
            vertexDegrees[id3]++;
            vertexDegrees[id4]++;

            // perform the flip
            map.reattachEdge(edge,edge->getPrevious()->getPrevious());
            map.reattachEdge(adjEdge,adjEdge->getPrevious()->getPrevious());

            // Keep track of accepted attempts to determine acceptance rate later
            accepted++;
        }
    }
    // flip the edge the other way
    else{
        id3 = edge->getPrevious()->data().vertexId;
        id4 = edge->getAdjacent()->getPrevious()->data().vertexId;
        double acceptProb = acceptanceProbability(vertexDegrees, vertexMasses, id1, id2, id3, id4);

        if(acceptProb >= 1 || uniform_real(rng, 0.0, 1.0) < acceptProb)
        {
            // update degrees of affected vertices
            vertexDegrees[id1]--;
            vertexDegrees[id2]--;
            vertexDegrees[id3]++;
            vertexDegrees[id4]++;

            map.reattachEdge(edge,edge->getPrevious());
            map.reattachEdge(adjEdge,adjEdge->getPrevious());

            accepted++;
        }
    }

	return true;
}

// subdivide square to which edge belongs into three squares
void splitSquare(Map<EdgeData> & map, EdgeHandle edge)
{
	EdgeHandle otheredge1 = edge->getNext();
	EdgeHandle otheredge2 = edge->getPrevious();
	map.insertFace(edge,4);
	map.insertEdge(otheredge2, otheredge1->getPrevious());
}


// Assign vertex ids to the edges in the map and return number of vertices.
int assignVertexIds(Map<EdgeData> & map)
{
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		(*it)->data().vertexId = -1;
	}

	int nextId = 0;
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		if( (*it)->data().vertexId == -1 )
		{
			EdgeHandle vertexEdge = *it;
			do {
				vertexEdge->data().vertexId = nextId;
				vertexEdge = vertexEdge->getAdjacent()->getNext();
			} while (vertexEdge != *it);
			nextId++;
		}
	}
	return nextId;
}

// write map to file in simple space-separated format
bool exportMap(Map<EdgeData> & map, std::string outputfile)
{
	std::ofstream file(outputfile.c_str());

	if( !file ) return false;

	file << map.numHalfEdges() << "\n";

	file << map.getRoot()->getId() << "\n";

	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		file << (*it)->getId() << " "
			 << (*it)->getAdjacent()->getId() << " "
			 << (*it)->getRotateCCW()->getId() << "\n";
	}

	return true;
}

// clears the contents of an output file before appending data to it.
void clearFile(std::string filename)
{
    std::ofstream file;
    file.open(filename, std::ofstream::out | std::ofstream::trunc);
    file.close();
}

// Export data relevant to reproduce the simulation to a file.
bool exportInfo(std::string fileName, int startMass, int numSims, int massFactor, Map<EdgeData> & map, int numSweeps, int numDataPoints)
{
    std::ofstream file;
    file.open(fileName, std::ofstream::out | std::ofstream::trunc);
    file.close();
    file.open(fileName, std::ios::app);
    if( !file ) return false;

    file << "Number of vertices: " << map.numVertices() << "\n";
	file << "Number of simulations: " << numSims << "\n";
	file << "Number of sweeps: " << numSweeps << "\n";
	file << "Observable measured " << numDataPoints << " times per total mass.\n";
	file << "Starting mass: " << startMass << "\n";
	file << "Mass increment: " << massFactor << "\n";

	return true;
}

// Export the values of an observable and its autocorrelation time
template <typename T>
bool exportObservable(std::vector<T> observable, std::string fileName, int numDataPoints)
{
    std::ofstream file;
    file.open(fileName, std::ios::app);
	if( !file ) return false;

    int autocorrelationTime = autocorrTime(observable, numDataPoints);
    double autocov0 = autocovariance(observable, numDataPoints)[0];
    double mean = sampleMean(observable);
    double error = statError(observable, autocorrelationTime, autocov0);
    file << mean << " " << error <<"\n";

	return true;
}

void updateHist(Map<EdgeData> & map, std::vector<int> & hist, std::vector<int> vec, int maximum)
{
    int entry;
    for(int i=0; i<map.numVertices(); i++)
    {
        entry = vec[i];
        if(entry>=0 && entry<maximum){hist[entry]++;}
    }
}

// export a histogram to a text file
template <typename T>
bool printHist(std::vector<T> & hist, std::string fileName)
{
    std::ofstream file;
    file.open(fileName, std::ios::app);
    if( !file ) return false;

    for(int i=0; i<int(hist.size()); i++)
    {
        file << hist[i] << " ";
        hist[i] = 0;
    }
    file << "\n";

	return true;
}

// write a line to a separate debug file that gets updated while the code is running
void writeToDebug(std::string output_string)
{
    std::ofstream debugFile;
    debugFile.open("DebugFile.txt", std::ios::app);
    debugFile << output_string;
}

// read map from file in simple space-separated format
bool importMap(std::string inputfile, Map<EdgeData> & map)
{
	std::ifstream file(inputfile.c_str());

	if( !file )
		return false;

	map.clearMap();

	int size, root;
	file >> size >> root;

	while( map.numHalfEdges() < size )
	{
		map.newEdge();
	}
	map.setRoot(map.getEdge(root));

	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		int next, adj;
		file >> next >> adj;
		EdgeHandle nextEdge = map.getEdge(next);
		(*it)->setNext( nextEdge );
		nextEdge->setPrevious( *it );
		(*it)->setAdjacent( map.getEdge(adj) );
	}
	return true;
}

template<typename RNG>
EdgeHandle RandomEdge(Map<EdgeData> & map, RNG & rng)
{
	return map.getEdge(uniform_int(rng,map.numHalfEdges()));
}

// Determine graph distance to the vertex corresponding to the starting
// point of origin. The result is saved in data().distance.
void assignGraphDistance(Map<EdgeData> & map, EdgeHandle origin)
{
	// initialize all distances to -1
	std::queue<EdgeHandle> queue;
	for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
	{
		(*it)->data().distance = -1;
	}

	// set distance to 0 on all edges sharing same starting point
	EdgeHandle edge = origin;
	do {
		edge->data().distance = 0;
		queue.push(edge);
		edge = edge->getRotateCCW();
	} while( edge != origin);

	// do breadth-first search
	while( !queue.empty() )
	{
		edge = queue.front();
		queue.pop();
		if( edge->getAdjacent()->data().distance == -1 )
		{
			// the end point of edge has not been visited yet, so
			// assign distance one greater to the corresponding vertex
			EdgeHandle nextEdge = edge->getAdjacent();
			do {
				nextEdge->data().distance = edge->data().distance + 1;
				queue.push(nextEdge);
				nextEdge = nextEdge->getRotateCCW();
			} while( nextEdge != edge->getAdjacent() );
		}
	}
}

// Determine the degrees of all vertices in the map
void assignVertexDegrees(Map<EdgeData> & map, std::vector<int> & vertexDegrees)
{
    for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
    {
        vertexDegrees[(*it)->data().vertexId] = map.vertexDegree((*it));
    }
}

// distribute the vertex mass evenly among all vertices
void assignVertexMass(Map<EdgeData> & map, int startMass, std::vector<int> & vertexMasses)
{
    int N, M, delta, V;
    V = map.numVertices();
    N = int(startMass/V);
    M = N*V;
    delta = startMass-M;

    for(int i=0; i<V-delta; i++)
    {
        vertexMasses[i] = N;
        startMass -= N;
    }

    for(int i=V-delta; i<V; i++)
    {
        vertexMasses[i] = N+1;
        startMass -= N+1;
    }
}

// compute the number of simulation runs given an initial and final mass and step size
int numSims(int startMass, int endMass, int massAdd)
{
    if((endMass-startMass)%massAdd == 0){return (endMass-startMass)/massAdd;}
    else{return double(endMass-startMass)/massAdd+1;}
}

// check if the map is a quadrangulation (all faces have four edges)
bool isQuadrangulation(Map<EdgeData> & map)
{
    std::vector<bool> visited(map.numHalfEdges(),false);
    bool isQuadrangle = true;
    int i;
    for(Map<EdgeData>::EdgeIterator it=map.begin();it!=map.end();it++)
    {
        if( !visited[(*it)->getId()] )
        {
            EdgeHandle edge = (*it);
            i=0;
            do
            {
                visited[edge->getId()] = true;
                edge = edge->getNext();
                i++;
            }while(edge != (*it));

            if(i!=4)
            {
                std::cout << "Degree of problematic face: " << i << "\n";
                isQuadrangle = false;
            }
        }
    }
    return isQuadrangle;
}

int main(int argc, char **argv)
{
    // Write some information to a file to aid in debugging.
    // Clear file first.
    clearFile("DebugFile.txt");
    writeToDebug("Start of setup process.\n");

	// initialize random number generator
	Xoshiro256PlusPlus rng(getseed());

	// initialize empty map with edges carrying the data of EdgeData
	planar::Map<EdgeData> map;

	// make a simple quadrangulation consisting of 2 squares glued along their perimeter
	map.makePolygon(4);

	// take in simulation arguments
	int numTriangles    = std::stoi(argv[1]);
	int startMass       = std::stoi(argv[2]);
    int maxMass         = std::stoi(argv[3]);
	int massAdd         = std::stoi(argv[4]);
	int numSweeps       = std::stoi(argv[5]);
	int numDataPoints   = std::stoi(argv[6]);
    int burninSweeps    = std::stoi(argv[7]);

	for(int n=2;n < numTriangles;n+=2)
	{
		// split random square (increasing number of squares by 2)
		EdgeHandle randomEdge = RandomEdge(map,rng);
		splitSquare(map,randomEdge);
	}

    assignVertexIds(map);

    // initialize vector to hold degrees of vertices
    std::vector<int> vertexDegrees(map.numVertices());
    std::vector<int> vertexMasses(map.numVertices());

    // intitialize vectors for holding the values of our observables
    int sims = numSims(startMass, maxMass, massAdd);
    int sweepsModulo = numSweeps/numDataPoints;     // after this number of sweeps, the observables are measured
    int histModulo = numSweeps/6;                   // after this number of sweeps, the histograms are exported
    std::cout << "Total number of data points: " << numDataPoints*sims << "\n";
    std::vector<double> observable1(numDataPoints);
    std::vector<int> observable2(numDataPoints);
    std::vector<double> observable3(numDataPoints);
    std::vector<int> observable4(numDataPoints);

    std::cout << "number of vertices: " << map.numVertices() << "\n";
	std::cout << "#halfedges = " << map.numHalfEdges() << ", #faces = "
		<< map.numFaces() << ", #vertices = " << map.numVertices() << "\n";

	// histogram of vertex degrees
	int maxHistDegree = 800;
	std::vector<int> degreeHistogram(maxHistDegree,0);
	std::string degreeHistogramFile = "histograms/Histogram vertex degrees (mass range "+std::to_string(startMass)+"-"+std::to_string(maxMass)+").txt";

	// histogram of vertex mass
	int maxHistMass = 600;
    std::vector<int> massHistogram(maxHistMass,0);
	std::string massHistogramFile = "histograms/Histogram vertex mass (mass range "+std::to_string(startMass)+"-"+std::to_string(maxMass)+").txt";

	// Clear existing histogram files before writing to them
	clearFile(degreeHistogramFile);
    clearFile(massHistogramFile);

    // make files for each of the observables and write basic simulation info
    std::string obsFile1 = "observables/sum of square degrees (mass range "+std::to_string(startMass)+"-"+std::to_string(maxMass)+").txt";
    std::string obsFile2 = "observables/maximum degree (mass range "+std::to_string(startMass)+"-"+std::to_string(maxMass)+").txt";
    std::string obsFile3 = "observables/sum of square masses (mass range "+std::to_string(startMass)+"-"+std::to_string(maxMass)+").txt";
    std::string obsFile4 = "observables/maximum mass (mass range "+std::to_string(startMass)+"-"+std::to_string(maxMass)+").txt";
    exportInfo(obsFile1, startMass, sims, massAdd, map, numSweeps, numDataPoints);
    exportInfo(obsFile2, startMass, sims, massAdd, map, numSweeps, numDataPoints);
    exportInfo(obsFile3, startMass, sims, massAdd, map, numSweeps, numDataPoints);
    exportInfo(obsFile4, startMass, sims, massAdd, map, numSweeps, numDataPoints);

    writeToDebug("Start of simulation loop.\n");

    short int index=0;
	for(int N=startMass; N<maxMass; N+=massAdd, index+=numDataPoints)
    {
        writeToDebug("\nSimulation: " + std::to_string((index/numDataPoints)+1) + "/" + std::to_string(sims) + "\n");

        // reset the map for each simulation
        map.clearMap();
        map.makePolygon(4);
        for(int n=2;n < numTriangles;n+=2)
        {
            // split random square (increasing number of squares by 2)
            EdgeHandle randomEdge = RandomEdge(map,rng);
            splitSquare(map,randomEdge);
        }
        assignVertexIds(map);
        assignVertexDegrees(map, vertexDegrees);
        for(int i=0; i<int(vertexDegrees.size()); i++)
        {
            std::cout << vertexDegrees[i] << ", ";
        }
        // reset vertex mass for each simulation
        assignVertexMass(map, N, vertexMasses);


        // perform a number of burn in sweeps
        for(int s=0;s<burninSweeps;++s)
        {
            // number of accepted attempts
            double accepted = 0;
            double accepted2 = 0;
            double accepted3 = 0;
            int largeMasses = 0;

            // flip numTriangles random edges = a single sweep
            for(int i=0;i<numTriangles;++i)
            {
                EdgeHandle randomEdge = RandomEdge(map,rng);
                flipEdge(map, randomEdge, rng, vertexDegrees, vertexMasses, accepted);
            }
            // move a random amount of mass between two vertices numVertices/2 times = a single sweep
            for(int i=0; i<round(map.numVertices()/2); i++)
            {
                moveMass(vertexDegrees, vertexMasses, rng, accepted2, accepted3, largeMasses, N);
            }

            std::cout << "Performed " << s << " burnin sweeps.\n";
        }

        writeToDebug("Start of actual sweeps.\n");

        for(int s=0;s<numSweeps;++s)
        {
            // number of accepted attempts
            double accepted = 0;
            double accepted2 = 0;
            double accepted3 = 0;
            int largeMasses = 0;

            // flip numTriangles random edges = a single sweep
            for(int i=0;i<numTriangles;++i)
            {
                EdgeHandle randomEdge = RandomEdge(map,rng);
                flipEdge(map, randomEdge, rng, vertexDegrees, vertexMasses, accepted);
            }
            // move a random amount of mass between two vertices numVertices/2 times = a single sweep
            for(int i=0; i<round(map.numVertices()/2); i++)
            {
                moveMass(vertexDegrees, vertexMasses, rng, accepted2, accepted3, largeMasses, N);
            }

            std::cout << "Performed " << s << " sweeps.\n";

            // update the histograms with the current configuration
            updateHist(map, degreeHistogram, vertexDegrees, maxHistDegree);
            updateHist(map, massHistogram, vertexMasses, maxHistMass);

            // calculate the acceptance rate of the mass and flip moves for the current sweep
            double acceptanceRate2 = 2.0*accepted2/(double)(map.numVertices());
            std::cout <<  "Acceptance rate mass move: " << acceptanceRate2 << ". \n";
            double acceptanceRate1 = accepted/(double)(numTriangles);
            std::cout <<  "Acceptance rate flip move: " << acceptanceRate1 << ". \n";

            // Every "sweepsModulo" sweeps, measure each of the observables
            if(s%sweepsModulo == 0 && s!=0)
            {
                // For each of the observables, add their value in this particular quadrangulation to the corresponding vector
                observable1[s/sweepsModulo] = squareSumDegree(vertexDegrees, map);
                observable2[s/sweepsModulo] = maxVertexDegree(vertexDegrees);
                observable3[s/sweepsModulo] = squareSumMass(vertexMasses, map);
                observable4[s/sweepsModulo] = maxVertexMass(vertexMasses);
            }
            // every "histModulo" sweeps, write the histograms to a file
            if(s%histModulo == 0 && s!=0)
            {
                // write the histograms to their respective files
                printHist(degreeHistogram, degreeHistogramFile);
                printHist(massHistogram, massHistogramFile);
            }
        }

        std::cout << "End of sweeps.\n";
        // at the end of the simulation for mass N, write the mean values of the observables with their errors to a file...
        exportObservable(observable1, obsFile1, numDataPoints);
        exportObservable(observable2, obsFile2, numDataPoints);
        exportObservable(observable3, obsFile3, numDataPoints);
        exportObservable(observable4, obsFile4, numDataPoints);
        std::cout << "Observables exported.\n";
    }

	// calculate the autocorrelation times of all observables and output them to the terminal
    int autocorr1, autocorr2, autocorr3, autocorr4;
    std::vector<double>slice1 = std::vector<double>(observable1.begin(), observable1.begin()+numDataPoints+1);
    std::vector<int>slice2 = std::vector<int>(observable2.begin(), observable2.begin()+numDataPoints+1);
    std::vector<double>slice3 = std::vector<double>(observable3.begin(), observable3.begin()+numDataPoints+1);
    std::vector<int>slice4 = std::vector<int>(observable4.begin(), observable4.begin()+numDataPoints+1);
    writeToDebug("Slices defined.\n");
    autocorr1 = autocorrTime(slice1, numDataPoints);
    autocorr2 = autocorrTime(slice2, numDataPoints);
    autocorr3 = autocorrTime(slice3, numDataPoints);
    autocorr4 = autocorrTime(slice4, numDataPoints);
    writeToDebug("Autocorrelation computed.\n");
    std::cout << "Autocorrelation time of \'sum of square degrees\': " << autocorr1 << "\n";
    std::cout << "Autocorrelation time of \'maximum degree\': " << autocorr2 << "\n";
    std::cout << "Autocorrelation time of \'sum of square masses\': " << autocorr3 << "\n";
    std::cout << "Autocorrelation time of \'maximum mass\': " << autocorr4 << "\n";

	// write map to file
	exportMap(map,"exports/export.txt");

	writeToDebug("Maps exported.\n");

	// write some information about the simulation to the terminal (mainly for debugging)
    std::cout << "\n\nnumber of vertices: " << map.numVertices() << "\n";
	std::cout << "#halfedges = " << map.numHalfEdges() << ", #faces = "
		<< map.numFaces() << ", #vertices = " << map.numVertices() << "\n";

    std::cout << "Is the map a planar map? " << map.isPlanarMap() << "\n";
    std::cout << "Is the map a quadrangulation? " << isQuadrangulation(map) << "\n";

    int sum = 0;
    for(int i=0; i<int(vertexMasses.size());i++)
    {
        sum+=vertexMasses[i];
    }
    std::cout << "Total vertex mass: " << sum << "\n";

	return 0;
}
