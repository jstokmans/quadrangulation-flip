# Quadrangulation flip

## Name
Quadrangulation flip

## Description
This project is part of a bachelor thesis at the department of High-Energy Physics at the Radboud University in Nijmegen. The thesis can be found at https://www.ru.nl/highenergyphysics/theses/bachelor-theses/ when finished. The code simulates random quadrangulations related to 2+1 dimensional Causal Dynnamical Triangulations. For more details, please see the thesis.
